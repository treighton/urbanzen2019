import Axios from "axios";

const getEndpoint = url => {
  const points = Axios.get(url).then(resp => {
    return resp.data;
  });
  return points;
};

export { getEndpoint };
